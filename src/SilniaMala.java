import java.util.Scanner;

/**
 * Created by pawel on 4/30/16.
 */
public class SilniaMala {
    public static long silnia(int n)
    {
        long s=1;
        for (int i =1;i<=n;i++)
        {
            s=s*i;
        }
        return s;
    }
    public static void main(String[] args)
    {
        System.out.println("Podaj dodatnia liczbe calkowita");
        Scanner skaner = new Scanner(System.in);
        int n = skaner.nextInt();
        System.out.printf("Silnia z %d wynosi %,d",n,silnia(n));

    }

}
