/**
 * Created by pawel on 4/22/16.
 */
public class Przekazywanie {
    public static void podwojenie(int[] x)
    {
        for (int i =0;i<x.length;i++)
        {
            x[i]=x[i]*2;
        }
    }
    public static void main(String[] args)
    {
        int[] x = new int[1];
        x[0] = 10;

        System.out.println("Wartos przed " + x[0]);
        podwojenie(x);
        System.out.println("Wartos po " + x[0]);

    }
}
