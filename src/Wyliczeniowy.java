/**
 * Created by pawel on 4/30/16.
 */
public class Wyliczeniowy {

    public enum DzienTygodnia
    {
        PONIEDZIALEK(1,"pon") ,WTOREK(2,"wt"),SRODA(3,"sr"),CZWARTEK(4,"czw"),PIATEK(5,"pt"),SOBOTA(6,"sob"),NIEDZIELA(7,"nd");
        private int numer;
        private String skrot;

        private DzienTygodnia(int n,String s)
        {
            numer=n;
            skrot=s;
        }
        public String toString()
        {
            return numer + "dzien tygodnia" + super.toString() + ", skrot : " + skrot;
        }

        public void komentuj()
        {
            if(this==DzienTygodnia.NIEDZIELA)
                System.out.println("Jest dzis Wolne");
            else if(this==DzienTygodnia.PONIEDZIALEK)
                System.out.println("Nie cierpie poniedzialkow");


        }
    }

    public static void main(String[] args)
    {
        DzienTygodnia dzis = DzienTygodnia.NIEDZIELA;
        DzienTygodnia popo = DzienTygodnia.PONIEDZIALEK;


        DzienTygodnia wtorek = DzienTygodnia.valueOf("PONIEDZIALEK");
        wtorek.komentuj();

//        for(DzienTygodnia d:DzienTygodnia.values())
//        {
//            System.out.println(d);
//        }
    }
}
