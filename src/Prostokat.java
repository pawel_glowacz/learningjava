/**
 * Created by pawel on 4/26/16.
 */
public class Prostokat extends Figura{


    private double bokA;
private double bokB;
    public Prostokat()
    {
        this(1.0,2.0);
    }
    public Prostokat(double a,double b)
    {
        this(a,b,"fioletowy",true);
    }
    public Prostokat(double a, double b, String k, boolean w)
    {
        super(k,w);
        bokA=a;
        bokB=b;
    }
    public double obliczPole()
    {
        return bokA*bokB;
    }
    public double obliczObwod()
    {
        return 2*(bokA+bokB);
    }
    public String toString()
    {
        return "Nazwa " + getClass().getName() + "// bokA: " + bokA + " bok B: " +bokB+ " //Pole: " + obliczPole() + " obwod: " + obliczObwod() + super.toString();
    }
public static void main(String[] args)
{
    Prostokat pr = new Prostokat(2,5);
    System.out.println(pr);
}

    @Override
    public void draw() {

    }
}
