import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by pawel on 4/30/16.
 */
public class BigNum {

    public static void main(String[] args)
    {
        int i;
        long l;
        double d;

        i=10000;
        l=10000000000L;
        d=1e09;

        BigInteger bi1,bi2,b3;
        BigDecimal bd;

        bi1 = new BigInteger("1000000000000000000000");
        bi2 = new BigInteger("1000000000000000000000");
        b3 = BigInteger.valueOf(l);

        System.out.println(bi1.multiply(bi2));
        System.out.println(bi1.add(bi2));
        System.out.println(bi1.divide(bi2));
    }
}
