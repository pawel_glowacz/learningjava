/**
 * Created by pawel on 4/26/16.
 */
public interface Drawable {
    public void draw();
}
