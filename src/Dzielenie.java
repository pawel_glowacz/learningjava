import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by pawel on 4/30/16.
 */
public class Dzielenie {

    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        boolean isNotCorrect = true;

        do {
            try {
                System.out.println("Wprowadz licznik");
                int l = skaner.nextInt();
                System.out.println("Wprowadz licznik");
                int m = skaner.nextInt();
                System.out.println("Wynik dzielenia " + l + " / " + m + " = " + (l / m));
                isNotCorrect = false;
            } catch (InputMismatchException ime) {
                //ae.printStackTrace();
                System.err.println("Zle dane wejsciowe");
                skaner.nextLine();
                System.out.println("Wprowadz dane ponownie");

            } catch (ArithmeticException ae) {
                //ae.printStackTrace();
                System.err.println("Nie wolno dzielic prez 0");
                System.out.println("Wprowadz dane ponownie");
            }
        }
        while (isNotCorrect);
        skaner.close();


    }
}
