/**
 * Created by pawel on 4/26/16.
 */
public class MojeKolo extends Kolo_new {
    public MojeKolo(double r)
    {
        super(r);
    }
//    public double obliczPole()
//    {
//        double r = getPromien();
//        return r*1000;
//    }
    // metody final nie moge byc przepisywane czyli zmieniane przez inne klasy
    public static void main(String[] args)
    {
        MojeKolo mk = new MojeKolo(10);
        System.out.println("Promien " + mk.getPromien());
        System.out.println("Pole " + mk.obliczPole());

    }
}
