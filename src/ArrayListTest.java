import java.util.ArrayList;

/**
 * Created by pawel on 4/26/16.
 */
public class ArrayListTest {
    public static void main(String[] args)
    {
        ArrayList<String> wyrazy = new ArrayList<String>();
        wyrazy.add("To");
        wyrazy.add("Jest");
        wyrazy.add("Nasza");
        wyrazy.add("Wspaniala");
        wyrazy.add("Lista");

        for (String s:wyrazy)
            System.out.println(s + " ");

        wyrazy.add(1,"nie");
        wyrazy.remove(3);
        for (String s:wyrazy)
            System.out.println(s + " ");

        System.out.println(wyrazy.contains("nasza"));
        System.out.println(wyrazy.indexOf("Lista"));
        System.out.println(wyrazy.get(0));
        System.out.println(wyrazy.set(2,"wasza"));
        System.out.println(wyrazy.size());
        wyrazy.clear();

        for (String s:wyrazy)
            System.out.println(s + " ");

        ArrayList<Integer> integery = new ArrayList<Integer>();

        integery.add(3);
        integery.add(4);

        for(int l:integery)
        System.out.println(l + " ");




    }
}
