import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by pawel on 4/30/16.
 */
public class TotekTest {

    public static void main(String[] args) {
        Scanner skaner = new Scanner(System.in);
        System.out.println("Wprowadz ilosc liczb do losowania");
        int k = skaner.nextInt();
        System.out.println("Wprowadz zakres od 1 do ...");
        int n = skaner.nextInt();

        ArrayList<Integer> test = new ArrayList<Integer>();
        ArrayList<Integer> twoje = new ArrayList<Integer>();

//        int[] test =new int[k];
//        int[] twoje =new int[k];

        for (int i = 0; i < k; i++) {
            System.out.println("Wprowadz liczbe nr. " + (i + 1));
            test.add(i, skaner.nextInt());
            //  test[i] = skaner.nextInt();
            if (test.get(i) <= n && test.get(i) > 0) {
                if (!twoje.contains(test.get(i))) {
                    twoje.add(i, test.get(i));
                } else {
                    System.out.println("Błąd! Ta liczba była już użyta");
                    i--;
                }
            } else {

                System.out.println("Błąd! Liczba przekracza zakres");
                i--;
            }

        }
        //  Arrays.sort(twoje);
        Collections.sort(twoje);
        System.out.println("Twoje liczby to:");
        //  System.out.println(Arrays.toString(twoje));
        for (Integer s : twoje)
            System.out.print(s + " ");

        System.out.println();

        int[] wyniki;
        wyniki = Totek.losuj(k, n);
        Arrays.sort(wyniki);
        System.out.println("Wylosowane:");
        System.out.println(Arrays.toString(wyniki));

        ArrayList<Integer> trafione = new ArrayList<Integer>();
        int trafienia = 0;
        for (int l = 0; l < k; l++) {
            if (twoje.contains(wyniki[l])) {
                trafienia++;
                trafione.add(l, wyniki[l]);
            }
        }
        System.out.println("Ilosc trafien: " + trafienia);

        if (trafienia > 0) {
            System.out.print("Trafione liczby: ");
            for (Integer s : trafione)
                System.out.print(s + " ");
        }


    }

}
