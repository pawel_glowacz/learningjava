/**
 * Created by pawel on 4/30/16.
 */
public class Kolory {
    public enum Kolor { Czerwony,Zielony,Niebieski,Zolty}
    public static void main(String[] args)
    {
       // String kolor = "Zielony";
        Kolor kolor = Kolor.Zolty;
        switch(kolor)
        {
            case Czerwony: System.out.println("Kolor Czerwony");break;
            case Niebieski: System.out.println("Kolor Niebieski");break;
            case Zielony: System.out.println("Kolor Zielony");break;
            default: System.out.println("Inny koloer");break;
        }
    }
}
