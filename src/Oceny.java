import java.util.Scanner;

/**
 * Created by pawel on 4/30/16.
 */
public class Oceny {

    public static void main(String[] args)
    {
        Scanner skaner = new Scanner(System.in);
        System.out.println("Wprowadz oceny / q=konczy wprowadzanie");
        int ocena;
        int jedynki,dwojki,trojki,czworki,piatki,szostki,inne;
        jedynki=dwojki=trojki=czworki=piatki=szostki=inne = 0;
        while (skaner.hasNextInt())
        {
            ocena=skaner.nextInt();
            switch(ocena)
            {
                case 1: jedynki++;break;
                case 2: dwojki++;break;
                case 3: trojki++;break;
                case 4: czworki++;break;
                case 5: piatki++;break;
                case 6: szostki++;break;
                default: inne++;break;

            }
        }

        System.out.println("Liczba jedynek: " + jedynki);
        System.out.println("Liczba dwojek: " + dwojki);
        System.out.println("Liczba trojek: " + trojki);
        System.out.println("Liczba czworek: " + czworki);
        System.out.println("Liczba piatek: " + piatki);
        System.out.println("Liczba szostek: " + szostki);
        System.out.println("Liczba innych: " + inne);
    }
}
