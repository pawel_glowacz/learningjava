/**
 * Created by pawel on 4/25/16.
 */
public class Kolo_new extends Figura {

        private double promien;

    public Kolo_new()
    {
        this(1.0);
    }
    public Kolo_new(double r)
    {
        this(r,"granatowy",true);
    }
    public Kolo_new(double r, String k, boolean w)
    {
        super(k,w);
        promien = r;
    }
    public double getPromien()
    {
        return promien;
    }
    public final double obliczObwod()
    {
        return 2*Math.PI*promien;
    }
    public final double obliczPole()
    {
        return Math.PI*promien*promien;
    }
    public String toString()
    {
        return "Nazwa " + getClass().getName() + " Promien" + promien + "obwod: " + obliczObwod() + "POle: " + obliczPole() + super.toString();
    }
public static void main(String[] args)
{
    Kolo_new kolo = new Kolo_new(2.4,"Zielony",false);
    System.out.println(kolo);
}

    @Override
    public void draw() {
        System.out.println("Rysuje kolo");
    }
}
