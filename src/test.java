public class test {

    private double promien;
    private int id;
    private static int nextId;

    public test()
    {
        this(1.0);
    }
    public test(double r)
    {
        this(r, "cos");
    }
    public test(double r, String j)
    {
        id=nextId;
        nextId++;
        promien=r;
        if(j.equals("km"))
        {
            promien=promien*1000;
        }
    }
    public void setPromien(double r)
    {
        promien=r;
    }
    public double getPromien()
    {
        return promien;
    }
    public int getId()
    {
        return id;
    }


    public static void main(String[] args)
    {
        test[] jeden = new test[3];
        jeden[0]=new test();
        jeden[1]=new test(5);
        jeden[2]=new test(2,"km");

        for (test x:jeden) {
            System.out.println(x.getId());
            System.out.println(x.getPromien());

            System.out.println(x.oblicz());
        }
    }


    public double oblicz()
    {

        return promien*promien;
    }
    static
    {
        nextId=1;
    }
}
