/**
 * Created by pawel on 4/26/16.
 */
public abstract class Figura implements Areable,Drawable {
    String kolor;
    boolean wypelnienie;

    public Figura()
    {
        this("czarny", false);
    }
    public Figura(String k,boolean w)
    {
        kolor = k;
        wypelnienie = w;
    }
    public String toString()
    {
        return " Kolor " + kolor + " Wypelnienie " + wypelnienie;
    }

    public static void main(String[] args)
    {

    }

//    public abstract double obliczPole();
//    public abstract double obliczObwod();
}
